# Gricey's Garry's Mod TTT End of Round Music Script

A simple Garry's Mod addon which plays a random sound at the end of a round of Trouble in Terrorist Town

## Usage

1. Copy the lua file into your server's `lua/autorun` directory
2. Put GMod compatible `.mp3` files into the server's `sound/endroundmusic` directory

Note that for an MP3 to be compatible with GMod, it should have a 44100Hz sample rate
