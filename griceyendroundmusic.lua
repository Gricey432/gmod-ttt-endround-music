GriceyTTTEndRoundMusic = {}

function GriceyTTTEndRoundMusic:Initialise()
	local endRoundMusicDir = "endroundmusic"
	
	if SERVER then
		-- Server
		GriceyTTTEndRoundMusic.tracks = {}
		
		-- Find all music files
		if (file.Exists("sound/"..endRoundMusicDir, "GAME")) then
			local files, dirs = file.Find("sound/"..endRoundMusicDir.."/*", "GAME")
			for _, filename in pairs (files) do
				if (string.GetExtensionFromFilename(filename) == "mp3") then
					-- AddFile takes a path with sound/ at the start while PlaySound takes one without sound/ at the start
					local path = endRoundMusicDir.."/"..filename;
					resource.AddFile("sound/"..path)
					table.insert(GriceyTTTEndRoundMusic.tracks, path)
				end
			end
		end

		util.AddNetworkString("GriceyTTTEndRoundMusic")

		hook.Add("TTTEndRound", "GriceyTTTEndRoundMusic",
			function(result)
				-- On TTT Round End
				
				-- Pick a random track
				local nTracks = #GriceyTTTEndRoundMusic.tracks
				if (nTracks > 0) then
					local trackPath = GriceyTTTEndRoundMusic.tracks[math.random(nTracks)]
					net.Start("GriceyTTTEndRoundMusic")
					net.WriteString(trackPath)
					net.Broadcast()
				end
			end
		)
	else
		-- Client
		net.Receive("GriceyTTTEndRoundMusic", function(_)
			local soundPath = net.ReadString()
			surface.PlaySound(soundPath)
		end)
	end
end

hook.Add("Initialize", "EndRoundMusicInit", GriceyTTTEndRoundMusic.Initialise)
